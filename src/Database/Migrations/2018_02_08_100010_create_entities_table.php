<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Kalnoy\Nestedset\NestedSet;

class CreateEntitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('newsdeeply.tables.entities'), function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('wp_entity_id')->nullable();
            $table->string('name');
            $table->string('slug');

            $table->index('slug');

            NestedSet::columns($table);
        });


        Schema::create(config('newsdeeply.tables.post_entities'), function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id')->unsigned();
            $table->integer('post_id')->unsigned();
            $table->integer('entity_id')->unsigned();

            $table->index('post_id');
            $table->index('entity_id');

            NestedSet::columns($table);

            $table->foreign('post_id')
                ->references('id')
                ->on(config('newsdeeply.tables.posts'))
                ->onDelete('cascade');

            $table->foreign('entity_id')
                ->references('id')
                ->on(config('newsdeeply.tables.entities'))
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(config('newsdeeply.tables.post_entities'), function (Blueprint $table) {
            $table->dropForeign(config('newsdeeply.tables.post_entities').'_post_id_foreign');
            $table->dropIndex(config('newsdeeply.tables.post_entities').'_post_id_index');

            $table->dropForeign(config('newsdeeply.tables.post_entities').'_entity_id_foreign');
            $table->dropIndex(config('newsdeeply.tables.post_entities').'_entity_id_index');

            NestedSet::dropColumns($table);
        });


        Schema::table(config('newsdeeply.tables.entities'), function (Blueprint $table) {
            $table->dropIndex(config('newsdeeply.tables.entities').'_slug_index');
            NestedSet::dropColumns($table);
        });

        Schema::dropIfExists(config('newsdeeply.tables.post_entities'));
        Schema::dropIfExists(config('newsdeeply.tables.entities'));
    }
}
