<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Kalnoy\Nestedset\NestedSet;

class CreateTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('newsdeeply.tables.topics'), function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('wp_topic_id')->nullable();
            $table->string('name');
            $table->string('slug');

            NestedSet::columns($table);
        });


        Schema::create(config('newsdeeply.tables.post_topics'), function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id')->unsigned();
            $table->integer('post_id')->unsigned();
            $table->integer('topic_id')->unsigned();

            NestedSet::columns($table);

            $table->index('post_id');
            $table->index('topic_id');

            $table->foreign('post_id')
                ->references('id')
                ->on(config('newsdeeply.tables.posts'))
                ->onDelete('cascade');

            $table->foreign('topic_id')
                ->references('id')
                ->on(config('newsdeeply.tables.topics'))
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(config('newsdeeply.tables.post_topics'), function (Blueprint $table) {
            $table->dropForeign(config('newsdeeply.tables.post_topics').'_post_id_foreign');
            $table->dropIndex(config('newsdeeply.tables.post_topics').'_post_id_index');

            $table->dropForeign(config('newsdeeply.tables.post_topics').'_topic_id_foreign');
            $table->dropIndex(config('newsdeeply.tables.post_topics').'_topic_id_index');

            NestedSet::dropColumns($table);
        });


        Schema::table(config('newsdeeply.tables.topics'), function (Blueprint $table) {
            NestedSet::dropColumns($table);
        });

        Schema::dropIfExists(config('newsdeeply.tables.post_topics'));
        Schema::dropIfExists(config('newsdeeply.tables.topics'));
    }
}
