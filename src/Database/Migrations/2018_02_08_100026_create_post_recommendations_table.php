<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Kalnoy\Nestedset\NestedSet;

class CreatePostRecommendationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('newsdeeply.tables.post_recommendations'), function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id')->unsigned();
            
            $table->integer('post_id')->unsigned();
            $table->string('title');
            $table->string('url');
            $table->string('publisher');
            $table->string('date_published');

            $table->index('post_id');
            $table->foreign('post_id')
                ->references('id')
                ->on(config('newsdeeply.tables.posts'))
                ->onUpdate('restrict')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(config('newsdeeply.tables.post_recommendations'), function($table) {
            $table->dropForeign(config('newsdeeply.tables.post_recommendations') . '_post_id_foreign');
            $table->dropIndex(config('newsdeeply.tables.post_recommendations') . '_post_id_index');
        });

        Schema::dropIfExists(config('newsdeeply.tables.post_recommendations'));
    }
}
