<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('newsdeeply.tables.posts'), function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id')->unsigned();

            $table->string('wp_post_id')->unique();
            $table->datetime('wp_post_modified_at');
            $table->datetime('wp_post_created_at');
            $table->string('status', 15)->nullable();
            $table->string('slug', 255)->unique();
            $table->string('link', 255);
            $table->string('image_url', 255)->nullable();

            $table->string('title', 255);
            $table->text('excerpt');
            $table->longText('content_rendered');


            /**
             * - WP post id
             * - WP post create date
             * - WP post last update date
             * - WP status - trash, publish, etc.
             * - title -
             * - body
             * - og unsa pa na sila haha
             * */

            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement('ALTER TABLE posts ADD FULLTEXT full(title, content_rendered)');


        Schema::create(config('newsdeeply.tables.contributors'), function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id')->unsigned();
            $table->string('wp_contributor_id')->nullable();
            $table->string('name');
            $table->string('slug');
            $table->string('image');
            $table->text('bio')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });


        Schema::create(config('newsdeeply.tables.post_contributors'), function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->integer('post_id')->unsigned();
            $table->integer('contributor_id')->unsigned();

            $table->index('contributor_id');
            $table->foreign('contributor_id')
                ->references('id')
                ->on(config('newsdeeply.tables.contributors'))
                ->onDelete('cascade');

            $table->index('post_id');
            $table->foreign('post_id')
                ->references('id')
                ->on(config('newsdeeply.tables.posts'))
                ->onDelete('cascade');
        });


        Schema::create(config('newsdeeply.tables.tags'), function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('slug')->unique();

            $table->timestamps();
            $table->softDeletes();
        });




        Schema::create(config('newsdeeply.tables.post_tags'), function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->integer('post_id')->unsigned();
            $table->integer('tag_id')->unsigned();

            $table->index('tag_id');
            $table->foreign('tag_id')
                ->references('id')
                ->on(config('newsdeeply.tables.tags'))
                ->onDelete('cascade');

            $table->index('post_id');
            $table->foreign('post_id')
                ->references('id')
                ->on(config('newsdeeply.tables.posts'))
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(config('newsdeeply.tables.post_contributors'), function($table) {
            $table->dropForeign('post_contributors_post_id_foreign');
            $table->dropIndex('post_contributors_post_id_index');
            $table->dropForeign('post_contributors_contributor_id_foreign');
            $table->dropIndex('post_contributors_contributor_id_index');
        });

        Schema::table(config('newsdeeply.tables.post_tags'), function($table) {
            $table->dropForeign('post_tags_post_id_foreign');
            $table->dropIndex('post_tags_post_id_index');
            $table->dropForeign('post_tags_tag_id_foreign');
            $table->dropIndex('post_tags_tag_id_index');
        });

        Schema::dropIfExists(config('newsdeeply.tables.post_tags'));
        Schema::dropIfExists(config('newsdeeply.tables.post_contributors'));
        Schema::dropIfExists(config('newsdeeply.tables.posts'));
        Schema::dropIfExists(config('newsdeeply.tables.contributors'));
        Schema::dropIfExists(config('newsdeeply.tables.tags'));
    }
}
