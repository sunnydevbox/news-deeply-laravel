<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Kalnoy\Nestedset\NestedSet;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('newsdeeply.tables.categories'), function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('wp_category_id')->nullable();
            $table->string('name');
            $table->string('slug');

            NestedSet::columns($table);
        });


        Schema::create(config('newsdeeply.tables.post_categories'), function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->integer('post_id')->unsigned();
            $table->integer('category_id')->unsigned();

            $table->index('post_id');
            $table->index('category_id');

            $table->foreign('post_id')
                ->references('id')
                ->on(config('newsdeeply.tables.posts'))
                ->onDelete('cascade');

            $table->foreign('category_id')
                ->references('id')
                ->on(config('newsdeeply.tables.categories'))
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(config('newsdeeply.tables.post_categories'), function (Blueprint $table) {
            $table->dropForeign(config('newsdeeply.tables.post_categories').'_post_id_foreign');
            $table->dropIndex(config('newsdeeply.tables.post_categories').'_post_id_index');

            $table->dropForeign(config('newsdeeply.tables.post_categories').'_category_id_foreign');
            $table->dropIndex(config('newsdeeply.tables.post_categories').'_category_id_index');
        });


        Schema::table(config('newsdeeply.tables.categories'), function (Blueprint $table) {
            NestedSet::dropColumns($table);
        });

        Schema::dropIfExists(config('newsdeeply.tables.post_categories'));
        Schema::dropIfExists(config('newsdeeply.tables.categories'));
    }
}
