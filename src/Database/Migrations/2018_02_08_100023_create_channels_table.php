<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Kalnoy\Nestedset\NestedSet;

class CreateChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('newsdeeply.tables.channels'), function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('wp_channel_id')->nullable();
            $table->string('name');
            $table->string('slug');

            NestedSet::columns($table);
        });


        Schema::create(config('newsdeeply.tables.post_channels'), function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->integer('post_id')->unsigned();
            $table->integer('channel_id')->unsigned();

            $table->index('post_id');
            $table->index('channel_id');

            $table->foreign('post_id')
                ->references('id')
                ->on(config('newsdeeply.tables.posts'))
                ->onDelete('cascade');

            $table->foreign('channel_id')
                ->references('id')
                ->on(config('newsdeeply.tables.channels'))
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(config('newsdeeply.tables.post_channels'), function (Blueprint $table) {
            $table->dropForeign(config('newsdeeply.tables.post_channels').'_post_id_foreign');
            $table->dropIndex(config('newsdeeply.tables.post_channels').'_post_id_index');

            $table->dropForeign(config('newsdeeply.tables.post_channels').'_channel_id_foreign');
            $table->dropIndex(config('newsdeeply.tables.post_channels').'_channel_id_index');
        });


        Schema::table(config('newsdeeply.tables.channels'), function (Blueprint $table) {
            NestedSet::dropColumns($table);
        });

        Schema::dropIfExists(config('newsdeeply.tables.post_channels'));
        Schema::dropIfExists(config('newsdeeply.tables.channels'));
    }
}
