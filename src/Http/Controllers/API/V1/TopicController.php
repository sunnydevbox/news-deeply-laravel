<?php
namespace Sunnydevbox\NewsDeeply\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\TWCore\Criteria\WhereNullCriteria;

class TopicController extends APIBaseController
{
    public function __construct(
		\Sunnydevbox\NewsDeeply\Repositories\Topic\TopicRepository $repository, 
		\Sunnydevbox\NewsDeeply\Validators\TopicValidator $validator,
		\Sunnydevbox\NewsDeeply\Transformers\TopicTransformer $transformer
	) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;

        $this->repository->pushCriteria(WhereNullCriteria::class);
	}
}