<?php
namespace Sunnydevbox\NewsDeeply\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class TagController extends APIBaseController
{
    public function __construct(
		\Sunnydevbox\NewsDeeply\Repositories\Tag\TagRepository $repository, 
		\Sunnydevbox\NewsDeeply\Validators\TagValidator $validator,
		\Sunnydevbox\NewsDeeply\Transformers\TagTransformer $transformer
	) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
	}
}