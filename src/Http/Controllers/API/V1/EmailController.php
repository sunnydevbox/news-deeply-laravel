<?php
namespace Sunnydevbox\NewsDeeply\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\NewsDeeply\Services\EmailService;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class EmailController extends APIBaseController
{
	public function process(Request $request)
	{
		$this->emailService->process($request->all());
		return $this->response->noContent();
	}

	public function __construct(EmailService $emailService)
	{
		$this->emailService = $emailService;
	}
}