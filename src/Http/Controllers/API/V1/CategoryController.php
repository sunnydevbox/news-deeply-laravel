<?php
namespace Sunnydevbox\NewsDeeply\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class CategoryController extends APIBaseController
{
    public function __construct(
		\Sunnydevbox\NewsDeeply\Repositories\Category\CategoryRepository $repository, 
		\Sunnydevbox\NewsDeeply\Validators\CategoryValidator $validator,
		\Sunnydevbox\NewsDeeply\Transformers\CategoryTransformer $transformer
	) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
	}
}