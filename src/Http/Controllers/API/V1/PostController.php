<?php
namespace Sunnydevbox\NewsDeeply\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\NewsDeeply\Services\PostService;
use Sunnydevbox\TWCore\Criteria\SearchRelationshipsCriteria;
use Sunnydevbox\TWCore\Criteria\FilterRelationshipsCriteria;
use Sunnydevbox\TWCore\Criteria\DateRangeCriteria;
use Sunnydevbox\NewsDeeply\Criteria\PostTypeCriteria;

class PostController extends APIBaseController
{
    public function __construct(
		\Sunnydevbox\NewsDeeply\Repositories\Post\PostRepository $repository, 
		\Sunnydevbox\NewsDeeply\Validators\PostValidator $validator,
		\Sunnydevbox\NewsDeeply\Transformers\PostTransformer $transformer
	) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;

		$this->repository->pushCriteria(PostTypeCriteria::class);
		$this->repository->pushCriteria(SearchRelationshipsCriteria::class);
        $this->repository->pushCriteria(FilterRelationshipsCriteria::class);
        $this->repository->pushCriteria(DateRangeCriteria::class);
	}


	public function import(
		PostService $postService, 
		Request $request
	) {
		return $postService->import($request->all());
	}
}