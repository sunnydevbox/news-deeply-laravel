<?php
namespace Sunnydevbox\NewsDeeply\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class EntityController extends APIBaseController
{
    public function __construct(
		\Sunnydevbox\NewsDeeply\Repositories\Entity\EntityRepository $repository, 
		\Sunnydevbox\NewsDeeply\Validators\EntityValidator $validator,
		\Sunnydevbox\NewsDeeply\Transformers\EntityTransformer $transformer
	) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
	}
}