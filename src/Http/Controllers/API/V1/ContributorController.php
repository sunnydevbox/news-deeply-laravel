<?php
namespace Sunnydevbox\NewsDeeply\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\TWCore\Criteria\FilterRelationshipsCriteria;

class ContributorController extends APIBaseController
{
    public function __construct(
		\Sunnydevbox\NewsDeeply\Repositories\Contributor\ContributorRepository $repository, 
		\Sunnydevbox\NewsDeeply\Validators\ContributorValidator $validator,
		\Sunnydevbox\NewsDeeply\Transformers\ContributorTransformer $transformer
	) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;

        $this->repository->pushCriteria(FilterRelationshipsCriteria::class);
	}
}