<?php
namespace Sunnydevbox\NewsDeeply\Services;

use GuzzleHttp;

class WPImportService
{
    private $response = null;

    protected $perPage = 100;
    protected $page = 1;
    protected $totalRecords = 0;
    protected $totalPages = 0;

    public function getNextPageUrl()
    {
        $url = $this->response->getHeaderLine('Link');

        $pos = strpos($url, 'rel="next"');
        if ($pos) {
            ++$this->page;
        } else {
            $this->page = null;
        }
    }

    public function request()
    {
        $client = new GuzzleHttp\Client();
        $url = 'http://app.newsdeeply.com/wp-json/wp/v2/posts?id=asd&per_page='.$this->perPage.'&page='.$this->page;
        echo $url . '<br>';
        $this->response = $client->get($url);
        $this->page++;
        $this->setTotalRecords();           

        return $this;
    }

    public function canCall()
    {
        return (is_null($this->response) || ($this->totalPages >= $this->page)) ? true : false;
    }

    public function setTotalRecords()
    {
        $this->totalRecords = (int) $this->response->getHeaderLine('X-WP-Total');
        $this->totalPages = ceil($this->totalRecords / $this->perPage);
    }

    public function getPageNumber()
    {
        return $this->page;
    }

    public function getBody()
    {
        return json_decode($this->response->getBody()->getContents(), true);
    }

    public function __construct()
    {

    }

}