<?php
namespace Sunnydevbox\NewsDeeply\Services;

use Sunnydevbox\NewsDeeply\Repositories\Topic\TopicRepository;
use Sunnydevbox\NewsDeeply\Repositories\PostTopic\PostTopicRepository;

class TopicService
{
	private $rpoTopic = null;
	private $rpoPostTopic = null;

	public function attach($post, $topics, $channels)
	{	
		if (!isset($topics[0])) {
			$topics = [$topics];
		}
		$post->pivotTopics()->detach();
		foreach($topics as $topic) {

			/**
			 * 1) Detach
			 * 2) Create the entity record.
			 * 3) Create the entity children record
			 * 4) attach the records to PostEntity
			 */
			if (!isset($topic['id']) || !isset($topic['name']) || !isset($topic['slug'])) {
				\Log::error('import_topic', ['post_id' => $post->wp_post_id]);
			}

			$attributes = [
				'wp_topic_id'	=> $topic['id'],
				'name'			=> $topic['name'],
				'slug'			=> $topic['slug'],
			];

			$parent = $this->rpoTopic->firstOrCreate($attributes);
			$parentPostTopic = $this->rpoPostTopic->firstOrCreate([
				'post_id'	=> $post->id,
				'topic_id'	=> $parent->id,
			]);

			foreach($channels as $channel) {
				$attributes = [
					'wp_topic_id'	=> $channel['id'],
					'name'			=> $channel['name'],
					'slug'			=> $channel['slug'],
				];
				
				$child = $parent->children()->firstOrCreate($attributes);
				$parentPostTopic->children()->firstOrCreate([
					'topic_id'	=> $child->id,
					'post_id'	=> $post->id,
				]);
			}
		}

		return $post;
	}

	public function __construct()
	{
		$this->rpoTopic = new TopicRepository(app());
		$this->rpoPostTopic = new PostTopicRepository(app());
	}
}


