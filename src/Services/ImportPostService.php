<?php
namespace Sunnydevbox\NewsDeeply\Services;

use Sunnydevbox\NewsDeeply\Services\TagService;
use Sunnydevbox\NewsDeeply\Services\ContributorService;
use Sunnydevbox\NewsDeeply\Services\EntityService;
use Sunnydevbox\NewsDeeply\Services\CategoryService;
use Sunnydevbox\NewsDeeply\Services\TopicService;
use Sunnydevbox\NewsDeeply\Services\PostRecommendationService;
use Sunnydevbox\NewsDeeply\Repositories\Post\PostRepository;

class ImportPostService
{
	protected $content_type 	= null;

	private $tagService 		= null;
	private $contributorService = null;
	private $entityService 		= null;
	private $categoryService 	= null;
	private $topicService 		= null;
	private $postRecommendationService = null;
	private $mode 				= 'create';
	
	private function mapping($requestData)
	{
		if (isset($requestData[0])) {
			foreach($requestData AS $rd) {
				$this->persistImport($rd);
			}
			return 'DONE IMPORTING';
		} else {
			return $this->persistImport($requestData);
		}
	}


	private function persistImport($data)
	{
		$data['post_type'] = $this->getContentType();

		if ($data['status'] == 'trash') {

			$post = $this->rpoPost->where('wp_post_id', $data['id'])->get();
			$post->delete();

		} else {

			if ($this->mode == 'update') {
				// CREATE the record ONLY when it does not create. 
				// Return the existing record if exists
				$post = $this->rpoPost->updateOrCreate(['wp_post_id' => $data['id']], $this->processMapping($data));

				// MARK Post as updated
				$post->touch();

			} else if ($this->mode == 'create') {

				// CREATE the record ONLY when it does not exists. 
				// Return the existing record if exists
				$post = $this->rpoPost->firstOrCreate($this->processMapping($data));
			}

			if (isset($data['terms'])) {
				// Tags
				if (isset($data['terms']['tags'])) {
					$post = $this->tagService->attachTags($post, $data['terms']['tags']);
				}

				// Categories
				if (isset($data['terms']['categories'])) {
					$post = $this->categoryService->attach($post, $data['terms']['categories']);
				}

				// Entities
				if (isset($data['terms']['entities'])) {
					$post = $this->entityService->attach($post, $data['terms']['entities']);
				}

				// Topics
				if (isset($data['terms']['topic'])) {
					$post = $this->topicService->attach($post, $data['terms']['topic'], $data['terms']['channels']);
				}

				// Contributor
				if (isset($data['terms']['contributor'])) {
					$post = $this->contributorService->attachContributors($post, $data['contributor']);
				}
			}

			// Topics 
			// From Executive Summary endpoint
			if (isset($data['topic_meta']) && !is_null($data['topic_id'])) {
				$post = $this->topicService->attach($post, $data['topic_meta'], []);
			}

			// RECOMMENDED READS
			if (isset($data['recommended_reads'])) {
				$post = $this->postRecommendationService->attach($post, $data['recommended_reads']);
			}
		
		} 
		return $post;
	}


	private function processMapping($request)
	{
		$data = [];
		$mapping = config('newsdeeply.mapping.post');

		foreach($mapping AS $tableColumn => $requestKey) {

			$keys = explode('.', $requestKey);

			$value = null;
			if (count($keys) == 1) {
				$requestKey = $keys[0];
				if (isset($request[$requestKey])) {
					$value = $request[$requestKey];
				}
			} else {
				$value = $request[$keys[0]];
				foreach(range(1, count($keys) - 1) as $key) {
					$value = $value[$keys[$key]];
				}
			}

			if ($value) {
				$data[$tableColumn] = $value;
			}
		}

		return $data;
	}

	public function import($requestData)
	{
		$data = $this->mapping($requestData);

		return $data;
	}

	public function setContentType($content_type)
	{
		$this->content_type = $content_type;
	}
	
	public function getContentType()
	{
		return $this->content_type;
	}

	public function update($requestData)
	{
		$this->mode = 'update';
		$data = $this->mapping($requestData);

		return $data;
	}

	public function __construct()
	{
		$this->tagService 			= new TagService;
		$this->contributorService 	= new ContributorService;
		$this->entityService 		= new EntityService;
		$this->categoryService 		= new CategoryService;
		$this->topicService 		= new TopicService;
		$this->topicService 		= new TopicService;
		$this->postRecommendationService = new PostRecommendationService;
	}
}