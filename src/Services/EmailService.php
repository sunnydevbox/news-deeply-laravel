<?php
namespace Sunnydevbox\NewsDeeply\Services;

use Mail;

class EmailService
{
    public function process($data)
    {
        Mail::send('newsdeeply::email.mail', ['body' => $data['text']], function ($m) use ($data) {
            $m->from($data['email'], $data['name']);

            $m->to(config('newsdeeply.email.from'), config('newsdeeply.email.name'))
                ->subject($data['subject']);
        });

        if(count(Mail::failures())) {
            throw new Exception("Failed to send email: " . join('; ', Mail::failures()), 1);
        }

        return true;
    }
}