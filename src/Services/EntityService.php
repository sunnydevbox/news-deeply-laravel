<?php
namespace Sunnydevbox\NewsDeeply\Services;

use Sunnydevbox\NewsDeeply\Repositories\Entity\EntityRepository;
use Sunnydevbox\NewsDeeply\Repositories\PostEntity\PostEntityRepository;

class EntityService
{
	private $rpoEntity = null;
	private $rpoPostEntity = null;

	public function attach($post, $entities)
	{
		$post->pivotEntities()->detach();
		foreach($entities as $entity) {

			/**
			 * 1) Detach
			 * 2) Create the entity record.
			 * 3) Create the entity children record
			 * 4) attach the records to PostEntity
			 */

			if (!isset($entity['id']) || !isset($entity['name']) || !isset($entity['slug'])) {
				\Log::error('import_entity', ['post_id' => $post->wp_post_id]);
			}
			
			$attributes = [
				'wp_entity_id'	=> $entity['id'],
				'name'			=> $entity['name'],
				'slug'			=> $entity['slug'],
			];

			// CREATE or RETURN the recrod in/from the entities table
			$parent = $this->rpoEntity->firstOrCreate($attributes);

			$parentPostEntity = $this->rpoPostEntity->firstOrCreate([
				'post_id'	=> $post->id,
				'entity_id'	=> $parent->id,
			]);

			foreach($entity['children'] as $childEntity) {
				$attributes = [
					'wp_entity_id'	=> $childEntity['id'],
					'name'			=> $childEntity['name'],
					'slug'			=> $childEntity['slug'],
				];
				
				$child = $parent->children()->firstOrCreate($attributes);
				$parentPostEntity->children()->firstOrCreate([
					'entity_id'	=> $child->id,
					'post_id'	=> $post->id,
				]);
			}
		}

		return $post;
	}

	public function __construct()
	{
		$this->rpoEntity 		= new EntityRepository(app());
		$this->rpoPostEntity 	= new PostEntityRepository(app());
	}
}

