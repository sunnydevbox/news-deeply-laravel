<?php
namespace Sunnydevbox\NewsDeeply\Services;

use Sunnydevbox\NewsDeeply\Repositories\Contributor\ContributorRepository;

class ContributorService
{
	private $rpoContributor = null;

	public function attachContributors($post, $contributors)
	{
		$savedContributors = [];
		if (gettype($contributors) == 'array') {
			foreach($contributors as $contributor) {
				if(!is_null($contributor['name'])) {
					$attributes = [
						'wp_contributor_id'	=> $contributor['id'],
						'name'				=> $contributor['name'],
						'slug'				=> $contributor['slug'],
						'image'				=> $contributor['image'],
						'bio'				=> $contributor['bio'],
					];

					$savedContributors[] = $this->rpoContributor->firstOrCreate($attributes)->id;
				}
			}	
		}
	
		$post->contributors()->sync($savedContributors);

		return $post;
	}

	public function __construct()
	{
		$this->rpoContributor = new ContributorRepository(app());
	}
}
