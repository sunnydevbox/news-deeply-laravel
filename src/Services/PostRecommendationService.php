<?php
namespace Sunnydevbox\NewsDeeply\Services;

use Sunnydevbox\NewsDeeply\Repositories\Post\PostRecommendationRepository;

class PostRecommendationService
{
	private $rpoPostRecommendation = null;

	public function attach($post, $recommendations)
	{
		if (!is_array($recommendations)) {
			return $post;
		}

		foreach($recommendations as $recommendation) {

			$attributes = [
				'post_id'		=> $post->id,
				'title'			=> $recommendation['title'],
				'url'			=> $recommendation['url'],
				'publisher'		=> $recommendation['publisher'],
				'date_published'=> $recommendation['date_published'],
			];

			$this->rpoPostRecommendation->firstOrCreate($attributes)->id;
		}

		return $post;
	}

	public function __construct()
	{
		$this->rpoPostRecommendation = new PostRecommendationRepository(app());
	}
}


