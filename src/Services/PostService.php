<?php
namespace Sunnydevbox\NewsDeeply\Services;

use Sunnydevbox\NewsDeeply\Repositories\Post\PostRepository;
use Sunnydevbox\NewsDeeply\Services\ImportPostService;

class PostService extends ImportPostService
{
	public $rpoPost 	= null;

	public function __construct(
		PostRepository $rpoPost
	) {
		$this->rpoPost 		= $rpoPost;

		parent::__construct();
	}
}
