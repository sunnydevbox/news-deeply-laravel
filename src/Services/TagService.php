<?php
namespace Sunnydevbox\NewsDeeply\Services;

use Sunnydevbox\NewsDeeply\Repositories\Tag\TagRepository;

class TagService
{
	private $rpoTag = null;

	public function attachTags($post, $tags)
	{
		$savedTags = [];
		foreach($tags as $tag) {

			$attributes = [
				'name'	=> $tag['name'],
				'slug'	=> $tag['slug'],
			];

			$savedTags[] = $this->rpoTag->firstOrCreate($attributes)->id;
		}

		$post->tags()->sync($savedTags);

		return $post;
	}

	public function __construct()
	{
		$this->rpoTag = new TagRepository(app());
	}
}


