<?php
namespace Sunnydevbox\NewsDeeply\Services;

use Sunnydevbox\NewsDeeply\Repositories\Category\CategoryRepository;

class CategoryService
{
	private $rpoCategory = null;

	public function attach($post, $categories)
	{
		$savedCategories = [];

		if (!isset($categories[0])) {
			$categories = [$categories];
		}

		foreach($categories as $category) {

			if (!isset($category['id']) || !isset($category['name']) || !isset($category['slug'])) {
				\Log::error('import_topic', ['post_id' => $post->wp_post_id]);
			}

			$attributes = [
				'wp_category_id'	=> $category['id'],
				'name'			=> $category['name'],
				'slug'			=> $category['slug'],
			];

			$node = $this->rpoCategory->create($attributes);

			if (isset($category['children'])) {
				foreach($category['children'] as $childCategory) {
					$attributes = [
						'wp_category_id'	=> $childCategory['id'],
						'name'			=> $childCategory['name'],
						'slug'			=> $childCategory['slug'],
					];

					$node->children()->create($attributes);
				}
			}

			$savedCategories[] = $node->id;
		}

		$post->categories()->detach();
		$post->categories()->attach($savedCategories);

		return $post;
	}

	public function __construct()
	{
		$this->rpoCategory = new CategoryRepository(app());
	}
}


