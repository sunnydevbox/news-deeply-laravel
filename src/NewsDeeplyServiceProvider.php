<?php
namespace Sunnydevbox\NewsDeeply;

use Sunnydevbox\TWCore\BaseServiceProvider;

class NewsDeeplyServiceProvider extends BaseServiceProvider
{

    /** OVERRIDE */
    public function mergeConfig()
    {
        return [
            __DIR__ . '/../config/config.php' => 'newsdeeply'
        ];
    }

    /** 
     * OVERRIDE 
     */
    public function loadRoutes()
    {
        return [
            realpath(__DIR__.'/../routes/api.php')
        ];
    }

    /**
     * OVERRIDE
     */
    public function loadViews()
    {
        return [
            __DIR__.'/../resources/views' => 'newsdeeply'
        ];
    } 

    public function registerCommands()
    {
        if ($this->app->runningInConsole()) {

            $this->commands([
                \Sunnydevbox\NewsDeeply\Console\Commands\MigrateCommand::class,
                \Sunnydevbox\NewsDeeply\Console\Commands\WPImportCommand::class,
                \Sunnydevbox\NewsDeeply\Console\Commands\UpdateLPostsCommand::class,
                \Sunnydevbox\NewsDeeply\Console\Commands\ImportExecutiveSummariesCommand::class,
            ]);
        }
    }

    public function registerProviders()
    {
        if (class_exists('\Sunnydevbox\TWCore\TWCoreServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\TWCore\TWCoreServiceProvider')
        ) {
            $this->app->register(\Sunnydevbox\TWCore\TWCoreServiceProvider::class);    
        }
    }
}