<?php
namespace Sunnydevbox\NewsDeeply\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Http\Request;
use Sunnydevbox\NewsDeeply\Services\ImportPostService;
use Sunnydevbox\NewsDeeply\Services\PostService;
use Sunnydevbox\NewsDeeply\Repositories\Post\PostRepository;

class WPImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newsdeeply:wpimport {--import=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Newsdeeply - run WP IMPORT';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {  
        $this->info('Running NewsDeeply Wordpress Import...');
    
        $baseImport = realpath(__DIR__.'/../../../shell/baseimport.sh');
        
        //shell_exec('chmod 755 ' . $baseImport);

        $storagePath    = storage_path() . '/app';
        $date           = date('Y-m-d-H:i:s');
        $fileFormat     = "$storagePath/ND-wpimport.$date-p";
        $baseUrl        = config('newsdeeply.wp.updateUrl') . 'posts';

        $headers = $this->getHeader($baseUrl);
        $totalPages = $headers['X-WP-TotalPages'];

        $result         = shell_exec ("$baseImport '$fileFormat' '$baseUrl' '$totalPages'");
        // $fileFormat     = "$storagePath/ND-wpimport.2018-02-22-02:27:39-p";
        // $result         = 50;

        $import         = $this->option('import');

        if ($import && $import == 'true') {
            for($i=1; $i<=(int)$result; $i++) {
                $file = $fileFormat.$i;
                echo $file ."\n";
                if (file_exists($file)) {

                    $string = file_get_contents($file);
                    $json_a = json_decode($string, true);
                    
                    $importPostService = new PostService(new PostRepository(app()));
                    $importPostService->import($json_a); 
                }
                
            }
        }
    }

    public function fire()
    {
        echo 'fire';
    }


    public function getHeader($url) 
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HEADER => 1,
            CURLOPT_NOBODY => 1,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ]);

        $response = curl_exec($curl);
        curl_close($curl);

        $h = explode("\n",$response);
        $headers = [];
        foreach($h as $part){
            $middle=explode(":",$part);
            if (isset($middle[1])) {
                $headers[trim($middle[0])] = trim($middle[1]);
            }
        }    

        return $headers;

    }
}