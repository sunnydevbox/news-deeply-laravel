<?php
namespace Sunnydevbox\NewsDeeply\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\Artisan;
use Sunnydevbox\NewsDeeply\Repositories\Post\PostRepository;
use Sunnydevbox\NewsDeeply\Services\PostService;
use Carbon\Carbon;

class UpdateLPostsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newsdeeply:updateposts {--datemodified=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Newsdeeply - Pull updates from the WP db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {  
        $this->info('Running NewsDeeply post update...');
    
        $baseImport = realpath(__DIR__.'/../../../shell/updatePosts.sh');
        
        shell_exec('chmod 755 ' . $baseImport);
        
        $storagePath    = storage_path() . '/app';
        $fileFormatDate           = date('Y-m-d-H:i:s');
        $fileFormat     = "$storagePath/ND-wpupdate.$fileFormatDate-p";
        $url            = config('newsdeeply.wp.updateUrl') . 'posts?id=0&per_page=100&modified_after=';

        // FORMAT: 2018-04-04T00:00:00
        // Y-m-d\T00:00:00
        $datemodified   = $this->option('datemodified');

        // GET THE LATEST MODIFIED DATE from posts
        $rpoPost        = new PostRepository(app());
        
        if ($datemodified) {
            $date = $datemodified;
        } else {
            $date           = $rpoPost->orderBy('wp_post_modified_at', 'DESC')->pluck('wp_post_modified_at')->first();
            $date           = Carbon::parse($date)->startOfDay()->format('Y-m-d\TH:i:s');
        }

        $url .= $date;

        $headers = $this->getHeader($url);
        $totalPages = $headers['X-WP-TotalPages'];

        $result         = shell_exec ("$baseImport '$fileFormat' '$url' '$totalPages'");
        
        /** DEBUG **/
        // $fileFormat = "$storagePath/ND-wpupdate.2018-06-19-08:39:45-p";
        // $result = 2;

        for($i=1; $i<=(int)$result; $i++) {
            $file = $fileFormat.$i;
            if (file_exists($file)) {

                $string = file_get_contents($file);
                $json_a = json_decode($string, true);
                
                $importPostService = new PostService(new PostRepository(app()));
                $importPostService->update($json_a); 
            }
            
        }
        
    }

    public function fire()
    {
        echo 'fire';
    }



    public function getHeader($url) 
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HEADER => 1,
            CURLOPT_NOBODY => 1,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ]);

        $response = curl_exec($curl);
        curl_close($curl);

        $h = explode("\n",$response);
        $headers = [];
        foreach($h as $part){
            $middle=explode(":",$part);
            if (isset($middle[1])) {
                $headers[trim($middle[0])] = trim($middle[1]);
            }
        }    

        return $headers;

    }
}