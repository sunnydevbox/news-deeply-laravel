<?php
namespace Sunnydevbox\NewsDeeply\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Http\Request;
use Sunnydevbox\NewsDeeply\Services\ImportPostService;
use Sunnydevbox\NewsDeeply\Services\PostService;
use Sunnydevbox\NewsDeeply\Repositories\Post\PostRepository;
use Carbon\Carbon;

class ImportExecutiveSummariesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newsdeeply:wpimport-executive-summaries {--datemodified=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Newsdeeply - run WP IMPORT for Executive Summary posts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {  
        $this->info('Running NewsDeeply Wordpress Import Executive Summaries...');
    
        $baseImport = realpath(__DIR__.'/../../../shell/import-executive-summaries.sh');
        
        //shell_exec('chmod 755 ' . $baseImport);

        $storagePath    = storage_path() . '/app';
        $date           = date('Y-m-d-H:i:s');
        $fileFormat     = "$storagePath/ND-executive-summaries.$date-p";
        $baseUrl        = config('newsdeeply.wp.updateUrl') . 'executive-summaries?';
        

        $datemodified   = $this->option('datemodified');

        // GET THE LATEST MODIFIED DATE from posts
        $rpoPost        = new PostRepository(app());
        
        if ($datemodified) {
            $date = $datemodified;
        } else {
            $date           = $rpoPost->makeModel()
                                ->where('post_type', 'executive-summary')
                                ->orderBy('wp_post_modified_at', 'DESC')
                                ->pluck('wp_post_modified_at')->first();
            $date           = Carbon::parse($date)->startOfDay()->format('Y-m-d\TH:i:s');
        }

        $baseUrl .= '&modified_after=' . $date;
        $headers = $this->getHeader($baseUrl);
        $totalPages = $headers['X-WP-TotalPages'];
        
        $result         = shell_exec ("$baseImport '$fileFormat' '$baseUrl' '$totalPages'");        
        
        /** !!! DEBUG !!! ***/
        
        // $result         = 1;
        // $fileFormat     = "$storagePath/ND-executive-summaries.2018-05-11-08:20:39-p";

        /** !!! End of DEBUG !!! ***/
    

        // $import         = $this->option('import');
        
        // if ($import && $import == 'true') {
            for($i=1; $i<=(int)$result; $i++) {
                $file = $fileFormat.$i;
                
                if (file_exists($file)) {
                    $string = file_get_contents($file);
                    $json_a = json_decode($string, true);
                    
                    $importPostService = new PostService(new PostRepository(app()));
                    $importPostService->setContentType('executive-summary');
                    $importPostService->update($json_a);
                }
                
            }
        // }
    }

    public function fire()
    {
        echo 'fire';
    }

    public function getHeader($url) 
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HEADER => 1,
            CURLOPT_NOBODY => 1,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ]);

        $response = curl_exec($curl);
        curl_close($curl);

        $h = explode("\n",$response);
        $headers = [];
        foreach($h as $part){
            $middle=explode(":",$part);
            if (isset($middle[1])) {
                $headers[trim($middle[0])] = trim($middle[1]);
            }
        }    

        return $headers;

    }
}