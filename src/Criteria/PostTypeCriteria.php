<?php
namespace Sunnydevbox\NewsDeeply\Criteria;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

/**
 * 
 * Override  TWCore's RequestCriteria 
 * to fit NewsDeeply's requirements
 * 
 */

class PostTypeCriteria implements CriteriaInterface
{
    /**
     * 
     * post_types={condition}:{value}
     * 
     * possible values for CONDITION
     * - ISNULL         ex: post_types=ISNULL
     * - NOTNULL        ex: post_types=NOTNULL
     * - NOT            ex: post_types=NOT:value1|value2|value3 ...
     * - EQ             ex: post_types=EQ:executive-summary
     *
     * @param [type] $model
     * @param RepositoryInterface $repository
     * @return void
     */
    public function apply($model, RepositoryInterface $repository)
    {
        
        $post_types = app('request')->get('post_types', null);
       
        if ($post_types) {

            $model->where(function($query) use ($post_types)
            {
                $types = explode(';', $post_types);

                foreach($types as $type) {
                    $details = explode(':', $type);
                    
                    $value = isset($details[1]) ? $details[1] : NULL;

                    $condition = strtoupper($details[0]);
                    
                    
                    if ($condition == 'ISNULL') {

                        $query->orWhereNull('post_type');

                    } else if ($condition == 'NOTNULL') {

                        $query->orWhereNotNull('post_type');

                    } else if ($condition == 'NOT') {
                        $value = explode('|', $value);

                        $query->orWhereNotIn('post_type', $value);
                    } else {
                        $condition = ($condition == 'EQ') ? '=' : $condition;
                            
                        $query->orWhere('post_type', $condition, $value);
                    }                
                }
            });
        }

        // print_r($model->getBindings()); dd($model->toSql());
        return $model;
    }
}