<?php
namespace Sunnydevbox\NewsDeeply\Repositories\Category;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class CategoryRepository extends TWBaseRepository
{
	protected $fieldSearchable = [
        'slug',
        'name',
    ];

	public function model()
	{
		return config('newsdeeply.models.category');
	}

}