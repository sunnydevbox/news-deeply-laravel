<?php
namespace Sunnydevbox\NewsDeeply\Repositories\PostTopic;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class PostTopicRepository extends TWBaseRepository
{

	public function model()
	{
		return config('newsdeeply.models.posttopic');
	}
}