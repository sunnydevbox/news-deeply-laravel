<?php
namespace Sunnydevbox\NewsDeeply\Repositories\Post;

use Sunnydevbox\NewsDeeply\Repositories\BaseRepository;

class PostRecommendationRepository extends BaseRepository
{
	protected $fieldSearchable = [
        'title',
        'publisher'
    ];

	public function model()
	{
		return config('newsdeeply.models.postrecommendation');
	}
}