<?php
namespace Sunnydevbox\NewsDeeply\Repositories\Post;

use Sunnydevbox\NewsDeeply\Repositories\BaseRepository;
use Sunnydevbox\NewsDeeply\Repositories\Post\PostImportTrait;

class PostRepository extends BaseRepository
{
	protected $fieldSearchable = [
        'title',
		'content_rendered',
		'post_type',
    ];

	public function model()
	{
		return config('newsdeeply.models.post');
	}
}