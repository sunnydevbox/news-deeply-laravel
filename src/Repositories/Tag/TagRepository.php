<?php
namespace Sunnydevbox\NewsDeeply\Repositories\Tag;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Sunnydevbox\NewsDeeply\Repositories\Post\PostImportTrait;

class TagRepository extends TWBaseRepository
{
	protected $fieldSearchable = [
        'slug',
        'name',
    ];

	public function create(array $attributes)
	{
		return $this->firstOrCreate($attributes);
	}


	public function model()
	{
		return config('newsdeeply.models.tag');
	}
}