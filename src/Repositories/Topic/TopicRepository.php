<?php
namespace Sunnydevbox\NewsDeeply\Repositories\Topic;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class TopicRepository extends TWBaseRepository
{
	protected $fieldSearchable = [
        'slug',
        'name',
    ];

	public function model()
	{
		return config('newsdeeply.models.topic');
	}

}