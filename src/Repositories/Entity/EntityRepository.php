<?php
namespace Sunnydevbox\NewsDeeply\Repositories\Entity;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class EntityRepository extends TWBaseRepository
{
	protected $fieldSearchable = [
        'slug',
        'name',
    ];

	public function model()
	{
		return config('newsdeeply.models.entity');
	}

}