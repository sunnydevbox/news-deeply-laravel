<?php
namespace Sunnydevbox\NewsDeeply\Repositories\PostEntity;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class PostEntityRepository extends TWBaseRepository
{

	public function model()
	{
		return config('newsdeeply.models.postentity');
	}

}