<?php
namespace Sunnydevbox\NewsDeeply\Repositories;

use Sunnydevbox\NewsDeeply\Criteria\RequestCriteria;
use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class BaseRepository extends TWBaseRepository
{
    //protected $cacheOnly = ['all', ];
    //or
    protected $cacheExcept = ['all', ];
    
    public function boot()
    {   
        // parent::boot();
        $this->pushCriteria(app('\Sunnydevbox\NewsDeeply\Criteria\RequestCriteria'));
    }
}