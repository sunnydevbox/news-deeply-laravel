<?php
namespace Sunnydevbox\NewsDeeply\Transformers;

use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
	public function transform($obj)
	{
		if (app('request')->get('filter')) {
			return $obj->toArray();
		}
		
		return [
			'id'	=> $obj->id,
		];
	}
}
