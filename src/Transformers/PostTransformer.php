<?php
namespace Sunnydevbox\NewsDeeply\Transformers;

use League\Fractal\TransformerAbstract;

class PostTransformer extends TransformerAbstract
{
	public function transform($obj)
	{
		if (app('request')->get('filter')) {
			return $obj->toArray();
		}

		return [
			'id'						=> $obj->id,
			'date'					=> $obj->date,
			'slug'					=> $obj->slug,
			'title'					=> $obj->title,
			'link'					=> $obj->link,
			'image'					=> $obj->image_url,
			'content'				=> $obj->content_rendered,
			'tags' 					=> $this->getTermNameSlug($obj->tags),
			'contributors' 	=> $this->getTermNameSlug($obj->contributors)
		];
	}

	public function getTermNameSlug($datas){
		$terms = [];
		foreach ($datas as $key => $data) {
			$terms[] = [
				'name' => $data->name,
				'slug' => $data->slug,
			];
		}

		return $terms;
	}
}
