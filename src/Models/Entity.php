<?php
namespace Sunnydevbox\NewsDeeply\Models;

use Kalnoy\Nestedset\NodeTrait;
use Sunnydevbox\TWCore\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class Entity extends Model
{
	use NodeTrait;

	protected $fillable = [
		'wp_entity_id',
		'name',
		'slug',
		'pivot',
	];

	protected $hidden = [
		'pivot',
		'wp_entity_id',
		'_lft',
		'_rgt',
	];

	public $columns = [
		'id',
		'name',
		'slug',
	];

	public $timestamps = false;

	public function posts()
	{
		return $this->belongsToMany(
			config('newsdeeply.models.post'),
			config('newsdeeply.tables.post_entities')
		);
	}


	
	public function getTable()
	{
		return config('newsdeeply.tables.entities');
	}
}