<?php
namespace Sunnydevbox\NewsDeeply\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\TWCore\Repositories\TWMetaTrait;
use Illuminate\Database\Eloquent\Model;

class PostRecommendation extends Model
{
	public $timestamps = false;

	protected $fillable = [
		'post_id',
		'title',
		'url',
		'publisher',
		'date_published',
	];

	protected $hidden = [
		'created_at',
		'hidden_at',
	];

	public function post()
	{
		return $this->belongsTo(
			config('newsdeeply.models.post')
		);
	}

	public function getTable()
	{
		return config('newsdeeply.tables.post_recommendations');
	}
}
