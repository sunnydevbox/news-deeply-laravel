<?php
namespace Sunnydevbox\NewsDeeply\Models;

use Kalnoy\Nestedset\NodeTrait;
use Sunnydevbox\TWCore\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
	use NodeTrait;

	protected $fillable = [
		'wp_topic_id',
		'name',
		'slug'
	];

	protected $hidden = [
		'pivot',
		'wp_topic_id',
		'_lft',
		'_rgt',		
	];

	public $timestamps = false;

	public function posts()
	{
		return $this->belongsToMany(
			config('newsdeeply.models.post'),
			config('newsdeeply.tables.post_topics')
		);
	}


	
	public function getTable()
	{
		return config('newsdeeply.tables.topics');
	}
}