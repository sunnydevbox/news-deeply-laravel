<?php
namespace Sunnydevbox\NewsDeeply\Models;

use Kalnoy\Nestedset\NodeTrait;
use Sunnydevbox\TWCore\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	use NodeTrait;

	protected $fillable = [
		'wp_category_id',
		'name',
		'slug'
	];

	protected $hidden = [
		'pivot',
		'wp_category_id',
		'_lft',
		'_rgt',
	];

	public $timestamps = false;

	public function posts()
	{
		return $this->belongsToMany(
			config('newsdeeply.models.post'),
			config('newsdeeply.tables.posts')
		);
	}


	
	public function getTable()
	{
		return config('newsdeeply.tables.categories');
	}
}