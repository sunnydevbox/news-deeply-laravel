<?php
namespace Sunnydevbox\NewsDeeply\Models;

use Kalnoy\Nestedset\NodeTrait;
use Sunnydevbox\TWCore\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class PostEntity extends Model
{
	use NodeTrait;

	protected $appends = [ 'name', 'slug', 'e_id'] ;

	protected $fillable = [
		'post_id',
		'entity_id',
	];

	protected $hidden = [
		'id',
		'post_id',
		'entity_id',
		'parent_id',
		'pivot',
		'_lft',
		'_rgt',
	];

	public $timestamps = false;

	/** 
	 * Entity ID (using 'e_id' so that it 
	 * will not conflict with the query)
	 * @return $entity->id
	 */
	public function getEIdAttribute()
	{	
		return $this->details->id;
	}

	/** 
	 * Entity Slug
	 * @return $entity->slug
	 */
	public function getSlugAttribute()
	{
		return $this->details->slug;
	}

	/** 
	 * Entity Name 
	 * @return $entity->name
	 */
	public function getNameAttribute()
	{
		return $this->details->name;
	}

	public function getDetailsAttribute()
	{
		return  $this->entity()->first();
	}

	public function entities()
	{	
		return $this->belongsTo(
			config('newsdeeply.models.entity'),
			'entity_id'
		);
	}

	public function entity()
	{
		return $this->belongsTo(
			config('newsdeeply.models.entity'),
			'entity_id'
		);
	}

	public function post()
	{
		return $this->belongsTo(
			config('newsdeeply.models.post'),
			'post_id'
		);
	}

	
	public function getTable()
	{
		return config('newsdeeply.tables.post_entities');
	}
}