<?php
namespace Sunnydevbox\NewsDeeply\Models;

use Kalnoy\Nestedset\NodeTrait;
use Sunnydevbox\TWCore\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class PostTopic extends Model
{
	use NodeTrait;

	protected $appends = [ 'name', 'slug', 't_id'] ;

	protected $fillable = [
		'post_id',
		'topic_id',
	];

	protected $hidden = [
		'id',
		'post_id',
		'topic_id',
		'parent_id',
		'pivot',
		'_lft',
		'_rgt',
	];

	public $timestamps = false;

	/** 
	 * Entity ID (using 'e_id' so that it 
	 * will not conflict with the query)
	 * @return $entity->id
	 */
	public function getTIdAttribute()
	{	
		if ($this->details) {
			return $this->details->id;
		}

		return null;
	}

	/** 
	 * Entity Slug
	 * @return $entity->slug
	 */
	public function getSlugAttribute()
	{
		if ($this->details) {
			return $this->details->slug;
		}

		return null;
	}

	/** 
	 * Entity Name 
	 * @return $entity->name
	 */
	public function getNameAttribute()
	{
		if ($this->details) {
			return $this->details->name;
		}

		return null;
	}

	public function getDetailsAttribute()
	{	
		return  $this->topics()->first();
	}

	public function topics()
	{	
		return $this->belongsTo(
			config('newsdeeply.models.topic'),
			'topic_id'
		);
	}

	// public function topic()
	// {
	// 	return $this->belongsTo(
	// 		config('newsdeeply.models.topic'),
	// 		'entity_id'
	// 	);
	// }

	public function post()
	{
		return $this->belongsTo(
			config('newsdeeply.models.post'),
			'post_id'
		);
	}

	
	public function getTable()
	{
		return config('newsdeeply.tables.post_topics');
	}
}