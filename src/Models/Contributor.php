<?php
namespace Sunnydevbox\NewsDeeply\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;


class Contributor extends Model
{
	protected $primaryKey = 'id';

	protected $fillable = [
		'wp_contributor_id',
		'name',
		'slug',
		'image',
		'bio'
	];

	protected $hidden = [
		'created_at',
		'deleted_at',
		'updated_at',
		'pivot',
	];

	public function getEntities()
    {
  		// select e.*
		// from entities e
		// 
		// join post_entities pe on pe.entity_id = e.id
		// 
		// join post_contributors pc on pe.post_id = pc.post_id
		// 
		// where pc.contributor_id = 2;

       	$relation = $this->posts();

	    $relation->getQuery()
	         ->join(config('newsdeeply.tables.post_entities').' as pe', 'pe.post_id', '=', config('newsdeeply.tables.post_contributors').'.post_id')
	         ->join(config('newsdeeply.tables.entities').' as e', 'e.id', 'pe.entity_id')
	         ->select(['e.id', 'e.name', 'e.slug'])
	         ->whereNotNull('e.parent_id')
	         ->groupBy('e.id')
	        ;

	    return $relation;
    }

	public function posts()
	{
		return $this->belongsToMany(
			config('newsdeeply.models.post'),
			config('newsdeeply.tables.post_contributors')
		);
	}

	public function getTable()
	{
		return config('newsdeeply.tables.contributors');
	}
}
