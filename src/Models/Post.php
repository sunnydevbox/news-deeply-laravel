<?php
namespace Sunnydevbox\NewsDeeply\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\TWCore\Repositories\TWMetaTrait;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	protected $fillable = [
		'wp_post_id',
		'wp_post_modified_at',
		'wp_post_created_at',
		'status',
		'slug',
		'link',
		'image_url',
		'title',
		'excerpt',
		'content_rendered',
		'post_type',
	];

	protected $hidden = [
		'created_at',
		'hidden_at',
	];

	public function contributors()
	{
		return $this->belongsToMany(
			config('newsdeeply.models.contributor'),
			config('newsdeeply.tables.post_contributors')
		);
	}

	public function tags()
	{
		return $this->belongsToMany(
			config('newsdeeply.models.tag'),
			config('newsdeeply.tables.post_tags')
		);
	}

	/* Used by the Nested Set implementation 
	 *	of the pivot table
	 */
	public function entities()
	{
		return $this->hasMany(
			config('newsdeeply.models.postentity')
		)->whereNull('parent_id');
	}

	/** For the many-to-many relationship */
	public function pivotEntities()
	{
		return $this->belongsToMany(
			config('newsdeeply.models.entity'),
			config('newsdeeply.tables.post_entities')
		);
	}

	public function categories()
	{
		return $this->belongsToMany(
			config('newsdeeply.models.category'),
			config('newsdeeply.tables.post_categories')
		);
	}

	/* Used by the Nested Set implementation 
	 *	of the pivot table
	 */
	public function topics()
	{
		return $this->hasMany(
			config('newsdeeply.models.posttopic')
		)->whereNull('parent_id');
	}

	/** For the many-to-many relationship */
	public function pivotTopics()
	{
		return $this->belongsToMany(
			config('newsdeeply.models.topic'),
			config('newsdeeply.tables.post_topics')
		);
	}

	public function channels()
	{
		return $this->belongsToMany(
			config('newsdeeply.models.channel'),
			config('newsdeeply.tables.post_channels')
		);
	}

	public function getTable()
	{
		return config('newsdeeply.tables.posts');
	}

	public function recommendations()
	{	
		return $this->hasMany(config('newsdeeply.models.postrecommendation'));
	}
}
