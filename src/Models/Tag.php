<?php
namespace Sunnydevbox\NewsDeeply\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;


class Tag extends Model
{
	protected $fillable = [ 'name', 'slug' ];

	protected $hidden = [
		'created_at',
		'deleted_at',
		'updated_at',
		'pivot',
	];

	public function posts()
	{
		return $this->belongsToMany(
			config('newsdeeply.models.post'),
			config('newsdeeply.tables.post_tags')
		);
	}
	
	public function getTable()
	{
		return config('newsdeeply.tables.tags');
	}
}