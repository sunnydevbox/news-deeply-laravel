#!/bin/sh
fileFormat=$1
url=$2
totalPages=$3
ctr=1
status="processing"

while [ "$status" != "DONE" ]
do
	output="$fileFormat$ctr"
	
	tempUrl="$url&page=$ctr"
	eval "curl '$tempUrl' > $output"
	# responseContent="$(echo $output | jq length $output)"
	## IF FILE IS EMPTY
	#if [ "$responseContent" -eq "0" ]
	if [ "$ctr" -gt "$totalPages" ]
	then
		eval "rm $output"
		status="DONE"
		echo $((ctr-1))
	fi
	ctr=$((ctr + 1))
done