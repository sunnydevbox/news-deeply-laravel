#!/bin/sh

fileFormat=$1
url=$2
totalPages=$3
per_page=100
ctr=1
status="processing"

while [ "$status" != "DONE" ]
do
	output="$fileFormat$ctr"
	#tempUrl="$url&id=asd&page=$ctr&per_page=$per_page"
	tempUrl="$url&id=asd&page=$ctr"

	#echo "PULLING: $((tempUrl))"
	eval "curl '$tempUrl' > $output"
	#responseContent="$(echo $output | jq length $output)"
	
	## IF FILE IS EMPTY
	#if [ "$responseContent" -eq "0" ]
	if [ "$ctr" -gt "$totalPages" ]
	then
		eval "rm $output"
		status="DONE"
		echo $((ctr-1))
	fi
	ctr=$((ctr + 1))
done