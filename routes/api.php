<?php


if (app()->getProvider('\Dingo\Api\Provider\LaravelServiceProvider')) 
{
	$api = app('Dingo\Api\Routing\Router');

	$api->version('v1', ['middleware' => []], function ($api) {
		$api->post('posts/import', config('newsdeeply.controllers.post') . '@import')->name('posts.import');
		$api->resource('posts', config('newsdeeply.controllers.post'));

		$api->resource('contributors', config('newsdeeply.controllers.contributor'));
		$api->resource('topics', config('newsdeeply.controllers.topic'));
		$api->resource('entities', config('newsdeeply.controllers.entity'));
		$api->resource('tags', config('newsdeeply.controllers.tag'));
		$api->resource('categories', config('newsdeeply.controllers.category'));
		$api->post('email', config('newsdeeply.controllers.email').'@process')->name('email.send');
	});
}