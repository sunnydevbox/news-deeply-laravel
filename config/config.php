<?php

return [
	'models' => [
		'post' 			=> 'Sunnydevbox\\NewsDeeply\\Models\\Post',
		'contributor' 	=> 'Sunnydevbox\\NewsDeeply\\Models\\Contributor',
		'tag' 			=> 'Sunnydevbox\\NewsDeeply\\Models\\Tag',
		'entity' 		=> 'Sunnydevbox\\NewsDeeply\\Models\\Entity',
		'category' 		=> 'Sunnydevbox\\NewsDeeply\\Models\\Category',
		'topic' 		=> 'Sunnydevbox\\NewsDeeply\\Models\\Topic',
		'postentity' 	=> 'Sunnydevbox\\NewsDeeply\\Models\\PostEntity',
		'posttopic' 	=> 'Sunnydevbox\\NewsDeeply\\Models\\PostTopic',
		'postrecommendation'	=> 'Sunnydevbox\\NewsDeeply\\Models\\PostRecommendation',
	],

	'controllers' => [
		'post' 			=> 'Sunnydevbox\\NewsDeeply\\Http\\Controllers\\API\\V1\\PostController',
		'contributor' 	=> 'Sunnydevbox\\NewsDeeply\\Http\\Controllers\\API\\V1\\ContributorController',
		'tag' 			=> 'Sunnydevbox\\NewsDeeply\\Http\\Controllers\\API\\V1\\TagController',
		'entity' 		=> 'Sunnydevbox\\NewsDeeply\\Http\\Controllers\\API\\V1\\EntityController',
		'category' 		=> 'Sunnydevbox\\NewsDeeply\\Http\\Controllers\\API\\V1\\CategoryController',
		'topic' 		=> 'Sunnydevbox\\NewsDeeply\\Http\\Controllers\\API\\V1\\TopicController',
		'email' 		=> 'Sunnydevbox\\NewsDeeply\\Http\\Controllers\\API\\V1\\EmailController',
	],

	'tables'	=> [
		'posts'				=> 'posts',
		'contributors'		=> 'contributors',
		'tags'				=> 'tags',
		'post_tags'			=> 'post_tags',
		'post_contributors'	=> 'post_contributors',
		'entities'			=> 'entities',
		'post_entities'		=> 'post_entities',
		'categories'		=> 'categories',
		'post_categories'	=> 'post_categories',
		'topics'			=> 'topics',
		'post_topics'		=> 'post_topics',
		'post_recommendations'	=> 'post_recommendations',

		// This will be removed in succeeding revisions
		'channels'			=> 'channels', 
		'post_channels'		=> 'post_channels',
	],


	'mapping'	=> [
		'post'	=> [
			// These are the array index that the parser will include 
			// in the sanitized data for the POST Model
			'wp_post_id'			=> 'id',
			'wp_post_modified_at'	=> 'modified',
			'wp_post_created_at'	=> 'date',
			'status'				=> 'status',
			'slug'					=> 'slug',
			'link'					=> 'link',
			'image_url'				=> 'image.url',
			'title'					=> 'title',
			'excerpt'				=> 'excerpt',
			'content_rendered'		=> 'content.rendered',
			'post_type'				=> 'post_type',
		]
	],

	'email' => [
		'name' 	=> 'Mailgun ND',
		'from'	=> 'flag-content@newsdeeply.com'
	],

	'wp' => [
		'import'	=> '',
		'updateUrl'	=> env('ND_WP_BASE_URL'),
	],
];
